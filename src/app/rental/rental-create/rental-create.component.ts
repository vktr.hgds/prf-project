import { Component, OnInit } from '@angular/core';
import { Rental } from '../shared/rental.model';
import { RentalService } from '../shared/rental.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-rental-create',
  templateUrl: './rental-create.component.html',
  styleUrls: ['./rental-create.component.css']
})
export class RentalCreateComponent implements OnInit {

	newRental: Rental;
	errors: any[] = [];

  constructor(private rentalService: RentalService, private router: Router) { }

  ngOnInit() {
  	this.newRental = new Rental();
  }

  handleImageChange() {
  	this.newRental.image = "https://www.hamburg-airport.de/images/Ski-Fluege_940.jpg";
  }

  createRental() {
  	//this.newRental.image = "https://www.hamburg-airport.de/images/Ski-Fluege_940.jpg";
  	//console.log(this.newRental);
  	this.rentalService.createRental(this.newRental).subscribe(
  		(rental: Rental) => {
  			this.router.navigate(['/rentals']);
  		},
  		(errorResponse: any) => {
  			this.errors = errorResponse.error.errors;
  		});
  }

}
