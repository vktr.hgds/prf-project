import { Component, OnInit, Input } from '@angular/core';
import { Rental } from '../../shared/rental.model';
import { Booking } from '../../../booking/shared/booking.model';
import { HelperService } from '../../../common/service/helper.service';
import { BookingService } from '../../../booking/shared/booking.service';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';


@Component({
  selector: 'bwm-rental-detail-booking',
  templateUrl: './rental-detail-booking.component.html',
  styleUrls: ['./rental-detail-booking.component.css']
})
export class RentalDetailBookingComponent implements OnInit {

	@Input() rental: Rental;
  newBooking: Booking;

  //refernce to modal - closes when cinfirmed
  modalRef: any;

  public daterange: any = {};
  bookedOutDates: any[] = [];
  errors: any[] = [];

  public options: any = {
      locale: { format: Booking.BOOKING_FORMAT },
      alwaysShowCalendars: false,
      opens: 'left',
      isInvalidDate: this.checkForInvalidDates.bind(this)
  };

  constructor(private helper: HelperService, 
              private modalService: NgbModal,
              private bookingService: BookingService) { }

  ngOnInit() {
    this.newBooking = new Booking();
    this.getBookedOutDates();
  }


  private checkForInvalidDates(date) {
    //if the array contians the booked dates, or the date is before today
    return this.bookedOutDates.includes(this.helper.formatBookingDate(date)) || date.diff(moment(), 'days') < 0;
    //return true;
  }
  
  private getBookedOutDates() {

    const bookings: Booking[] = this.rental.bookings;

    if (bookings && bookings.length > 0) {
        bookings.forEach((booking: Booking) => {
          console.log(booking);
          const DateRange = this.helper.getBookingRangeOfDates(booking.startAt, booking.endAt);

          //we need just the elements, not array of array
          this.bookedOutDates.push(...DateRange);
        //debugger;
      });
    }
  }


  private addNewBookedDates(bookingData: any) {
    const DateRange = this.helper.getBookingRangeOfDates(bookingData.startAt, bookingData.endAt);
    this.bookedOutDates.push(...DateRange);
  }

  openConfirmModal(content){
    this.errors = [];
    this.modalRef = this.modalService.open(content);
  }

  createBooking(){
    //console.log(this.newBooking);
    

    this.newBooking.rental = this.rental;
    this.bookingService.createBooking(this.newBooking).subscribe(
      (bookingData: any) => {
        this.addNewBookedDates(bookingData);
        this.newBooking = new Booking();
        this.modalRef.close();
      },
      (errorResponse: any) => {
        this.errors = errorResponse.error.errors;
      });
  }


  public selectedDate(value: any, datepicker?: any) {
      // this is the date the iser selected
      //console.log(value);

      // any object can be passed to the selected event and it will be passed back here

      this.newBooking.startAt = this.helper.formatBookingDate(value.start);
      this.newBooking.endAt = this.helper.formatBookingDate(value.end);

      //its in millisecond, we convert it into days
      this.newBooking.days = -(value.start.diff(value.end, 'days')); 
      this.newBooking.totalPrice = this.newBooking.days * this.rental.dailyRate;

      //console.log(this.newBooking);
      // or manupulat your own internal property
      this.daterange.start = value.start;
      this.daterange.end = value.end;
      this.daterange.label = value.label;
  }

}
