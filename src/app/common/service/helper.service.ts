import { Injectable } from '@angular/core';
import * as moment from 'moment';
import {Booking} from '../../booking/shared/booking.model';

@Injectable()
export class HelperService {

	public getRangeOfDates(startAt, endAt, dateFormat) {
		//startAt and endAt are just string at first, with moment we transform it
		const dateArray = [];

		const momentEndAt = moment(endAt);
		let momentStartAt = moment(startAt);

		while (momentStartAt < momentEndAt) {
			dateArray.push(momentStartAt.format(dateFormat));
			momentStartAt = momentStartAt.add(1, 'day');

			//count days form start to end -- days are end-start+1
		}

		dateArray.push(moment(startAt).format(dateFormat));
		dateArray.push(momentEndAt.format(dateFormat));

		return dateArray;

	}

	//DONT use this
	private formatDate(date, dateFormat) {
		return moment(date).format(dateFormat);
	}

	//dont use this
	public formatBookingDate(date) {
		return this.formatDate(date, Booking.BOOKING_FORMAT);
	}

	public getBookingRangeOfDates(startAt, endAt) {
		return this.getRangeOfDates(startAt, endAt, Booking.BOOKING_FORMAT);
	}
}