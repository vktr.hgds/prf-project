import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../shared/auth.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
	loginForm: FormGroup;
	errors: any[] = [];
	notifyMsg: string = '';
	login_loggedin: boolean = false;

  constructor(private fb: FormBuilder, 
  	private auth: AuthService, 
  	private router: Router,
  	private route: ActivatedRoute) { }

  ngOnInit() {
  	this.initForm();
  	//register successful or not
  	this.route.params.subscribe((params) => {
  		 if (params['register'] === 'success') {
  		 	this.notifyMsg = 'You have successfully registered. Login now.'
  		 }
  	});
  }

  initForm() {
  	this.loginForm = this.fb.group({
  		email: ['', Validators.required],
  		password: ['', Validators.required],
  	})
  }

  public isLoggedIn(): boolean {
  	return this.login_loggedin;
  }

  public setLoggedInFalse() {
  	this.login_loggedin = false;
  }

  login(){
  	console.log(this.loginForm.value)
  	this.auth.login(this.loginForm.value).subscribe(
  		(token) => {
  			this.login_loggedin = true;
  			this.router.navigate(['/rentals']);
  			
  		},
  		(errorRes) => {
  			 this.errors = errorRes.error.errors;
  		})
  }
}
