import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as moment from 'moment';
const jwt = new JwtHelperService();
//import {LoginComponent} from '../login/login.component';
import { map } from 'rxjs/operators';

class DecodedToken {
	exp: number = 0;
	username: string = '';
}

@Injectable()
export class AuthService {

private decodedToken;
loggedin: boolean = false;
 //var jwt = new JwtHelperService();

  constructor(private http: HttpClient){
  	this.decodedToken = JSON.parse(localStorage.getItem('bwm_meta')) || new DecodedToken();
  }//, private LC: LoginComponent) {}

  public register(registerData: any): Observable<any> {
  	return this.http.post('/api/v1/users/register', registerData);
  }

  public login(loginData: any): Observable<any> {
  	//if (this.LC.isLoggedIn) {
  		this.loggedin = true;
  	//}
  	
  	return this.http.post('/api/v1/users/auth', loginData).pipe(map(
  		(token: string) => this.saveToken(token)));
  }

  public logout () {
  	this.loggedin = false;
  	localStorage.removeItem('bwm_auth');
  	localStorage.removeItem('bwm_meta');
  	this.decodedToken = new DecodedToken();

  }


  public getUsername(): string {
  	return this.decodedToken.username;
  }


  private saveToken(token: string): string {
  	this.decodedToken = jwt.decodeToken(token);
  	localStorage.setItem('bwm_auth', token);
  	localStorage.setItem('bwm_meta', JSON.stringify(this.decodedToken));
  	return token;
  }


  public getExpiration() {
  	return moment.unix(this.decodedToken.exp);
  }

  public isAuthenticated(): boolean {
  	return moment().isBefore(this.getExpiration());
  }

  public getAuthToken(): string {
	  return localStorage.getItem('bwm_auth');
  }


}