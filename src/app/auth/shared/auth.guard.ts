
//this file checks if the person logged in or not
//this is necessary because we dont want the user to reach login and register pages when logged in
//this file is from angular official page

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {

  private url: string;

  constructor(private authService: AuthService, private router: Router) {}


  private handleAuthState(): boolean {
    //ha be van jelentkezve, de a loginra akar menni, akkor navigálja át a rentalokhoz
    if (this.isLoginOrRegister()) {
      this.router.navigate(['/rentals']);
      return false;
    }
    return true;
  }

  private handleNotAuthState(): boolean {
    if (this.isLoginOrRegister()) {
      return true;
    }
    
    this.router.navigate(['/login']);
    return false;
  }

  private isLoginOrRegister(): boolean {
    if (this.url.includes('login') || this.url.includes('register')) {
      return true;
    }
    return false;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.url = state.url;
    if (this.authService.isAuthenticated()) {
      return this.handleAuthState();
    }
    return this.handleNotAuthState();

  }

}