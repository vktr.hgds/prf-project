const Rental = require('./models/rental');
const User = require('./models/user');
const Booking = require('./models/booking')

class FakeDb {

	constructor() {
		this.rentals = [{
		  title: "Brand new ski - Atomic",
		  city: "Szeged",
		  street: "Alma street 10",
		  category: "Top category",
		  brand: "Atomic",
		  image: "https://www.hamburg-airport.de/images/Ski-Fluege_940.jpg",
		  description: "This is a top category ski. Not recommended for newbies",
		  dailyRate: 43
		  },
		  {
		  title: "Used ski - Head",
		  city: "Szeged",
		  street: "Szechenyi square 5",
		  category: "High category",
		  brand: "Head",
		  image: "https://www.hamburg-airport.de/images/Ski-Fluege_940.jpg",
		  description: "High category used ski",
		  dailyRate: 11
		  },
		  {
		  title: "New ski - Head",
		  city: "Szeged",
		  street: "Bartok square 5",
		  category: "Middle category",
		  brand: "Head",
		  image: "https://www.hamburg-airport.de/images/Ski-Fluege_940.jpg",
		  description: "Middle category used ski",
		  dailyRate: 11
		  }];

		  this.users = [{
		  	username: "Test User",
		  	email: "test@gmail.com",
		  	password: "testtest"
		  },{
		  	username: "Test User1",
		  	email: "test1@gmail.com",
		  	password: "testtest1" 
		  }];
	}

	async cleanDb() {
		await User.remove({});
		await Rental.remove({});
		await Booking.remove({});
	}

	pushDataToDb() {
		const user = new User(this.users[0]); 
		const user1 = new User(this.users[1]); 

		this.rentals.forEach((rental) => {
			const newRental = new Rental(rental);
			newRental.user = user;

			user.rentals.push(newRental);

			newRental.save();
		});

		user.save();
		user1.save();
	}

	async seedDb() {
		await this.cleanDb();
		this.pushDataToDb();
	}
}

module.exports = FakeDb;
