const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
	username: {
		type: String,
		min:[8, "Too short, minimum 8 characters are necessary"],
		max:[32, 'Too long, maximum 32 charackters are allowed'],
		required: 'Username is required'
	},
	email: {
		type: String,
		min:[8, "Too short, minimum 8 characters are necessary"],
		max:[32, 'Too long, maximum 32 charackters are allowed'],
		unique: true, //every email is unique
		lowercase: true,
		required: 'Email is required'
	},
	password: {
		type: String,
		min:[8, "Too short, minimum 8 characters are necessary"],
		max:[32, 'Too long, maximum 32 charackters are allowed'],
		required: 'Password is required'
	},
	//here comes the rentals connected to the specific person
	rentals: [{type: Schema.Types.ObjectId, ref: 'Rental'}],
	bookings: [{type: Schema.Types.ObjectId, ref: 'Booking'}]
	
});

//Check is password in db matches password in inputform
userSchema.methods.isSamePassword = function (reqPassword) {

	//compare the two hash-es, if same than retrun true
	return bcrypt.compareSync(reqPassword, this.password);
}


//create salt for password - store it in an encrypted way
userSchema.pre('save', function(next){

	const user = this;

	bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(user.password, salt, function(err, hash) {
        user.password = hash;
        next();
    });
}); 
});


module.exports = mongoose.model('User', userSchema );
