const User = require('../models/user');
const mongooseHelpers = require('../helpers/mongoose');
const jwt = require('jsonwebtoken');
const config = require('../config/dev');

//authentication
exports.auth = function(req, res) {
  const email = req.body.email;
  const password = req.body.password;

  if (!password || !email) {
  	 return res.status(422).send({errors: [{title: 'Login Error!', detail: 'Data is missing!'}]});
  }

  User.findOne({email}, function(err, user){
  	if (err) {
  		return res.status(422).send({errors: mongooseHelpers.normalizeErrors(err.errors)});
  	}

  	if (!user) {
  		return res.status(422).send({errors: [{title: 'Invalid username', detail: 'Username does not exist!'}]});
  	}

  	//if everything correct than log in
   	if (user.isSamePassword(password)) {

   		const token = jwt.sign({
		  userId: user.id,
		  username: user.username
		}, config.SECRET, { expiresIn: 60 * 60 });

   		//jwt token for auth - expires after an hour
   		return res.json(token)

   	} else {
   		return res.status(422).send({errors: [{title: 'Error occured!', detail: 'Wrong email or password!'}]});
   	}

  });
}


exports.register = function(req, res) {
  const username = req.body.username;
  const email = req.body.email;
  const password = req.body.password;
  const passwordAgain = req.body.passwordConfirmation;

  if (!username || !email) {
  	 return res.status(422).send({errors: [{title: 'Registration Error!', detail: 'Could not find username or email!'}]});
  }

  if (password != passwordAgain) {
  	 return res.status(422).send({errors: [{title: 'Registration Error!', detail: 'Passwords do not match!'}]});
  }

  //check if email exists
  User.findOne({email: email}, function (err, existingUser){
  	if (err) {
  		return res.status(422).send({errors: mongooseHelpers.normalizeErrors(err.errors)});
  	}

  	if (existingUser) {
  		return res.status(422).send({errors: [{title: 'Registration Error!', detail: 'Email is already in database!'}]});
  	}

  	const user = new User({
  		username, email, password
  	});


  	//if everything correct, than save it 
  	user.save(function(err){
  		if (err) {

  			//helper folder - mongoose error
  			return res.status(422).send({errors: mongooseHelpers.normalizeErrors(err.errors)});
  		}
  		return res.json({"registered": true});
  	})


  })
}

//check tokens - if not the same -> error
//if they are the same -> check if error occured -> return status 422
//if everythin fine -> next method - login successfull
exports.authMiddleware = function (req, res, next) {
	const token = req.headers.authorization;

	if (token) {
		const user = parseToken(token);

		User.findById(user.userId, function(err, user) {
			if (err) {
				 return res.status(422).send({errors: [{title: 'Not authorizated', detail: 'Token probably expired - Login again'}]});
			}

			if (user) {
				//this way we can pass our object to the next handler/middleware
				res.locals.user = user;
				next();
			} else {
				return notAuthorized(res);
			}
		});
	} else {
		return notAuthorized(res);
	}
}

function parseToken(token) {

	//change token to the final format - delete the convention from the start - split token

	return jwt.verify(token.split(' ')[1], config.SECRET);
}

//avoid repeatable status codes
function notAuthorized(res) {
	return res.status(401).send({errors: [{title: 'Not authorizated', detail: 'Token probably expired - Login again'}]});
}