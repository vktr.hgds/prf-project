const Booking = require('../models/booking');
const Rental = require('../models/rental');
const User = require('../models/user');
//const mongooseHelpers = require('../helpers/mongoose');
const moment = require('moment');

exports.createBooking = function(req, res) {
	const {startAt, endAt, totalPrice, days, rental} = req.body;
	const user = res.locals.user;

	const booking = new Booking({startAt, endAt, totalPrice, days});
 
 	//need the specific rental with its bookings and user
	Rental.findById(rental._id)
	      .populate('bookings')
	      .populate('user')
	      .exec(function(err, foundRental) {

	      	if (err) {
				return res.status(422).send({errors: [{title: 'Error occured', detail: 'Could not find booking'}]});
			}

			if (foundRental.user.id === user.id) {
				return res.status(422).send({errors: [{title: 'Invalid booking!', detail: 'Cannot create a booking on your own rental!'}]});
			}
 
			//if dates are correct
			if(isValidBooking(booking, foundRental)) {
				//this user makes the booking
				booking.user = user;
				booking.rental = foundRental;
				//push when valid
				foundRental.bookings.push(booking);

				booking.save(function(err){
					if (err) {
						return res.status(422).send({errors: [{title: 'Invalid booking!', detail: 'Could not save booking'}]});
					}
					foundRental.save();

					//update user with the booking he/she made
					User.update({_id: user.id}, {$push: {bookings: booking}}, function(){});

				});
				//updating foundrental
				
				
				return res.json({startAt: booking.startAt, endAt: booking.endAt});
			} else {
				return res.status(422).send({errors: [{title: 'Invalid booking!', detail: 'Chosen dates are already taken'}]});
			}
		});
}

//convert date to moment date 

//compare dates
function isValidBooking(proposedBooking, rental) {
	let isValid = true;

	if (rental.bookings && rental.bookings.length > 0) {
		isValid = rental.bookings.every(function(booking) {

			const proposedStart = moment(proposedBooking.startAt);
			const proposedEnd = moment(proposedBooking.endAt);
			const actualStart = moment(booking.startAt);
			const actualEnd = moment(booking.endAt);

			return ((actualStart < proposedStart && actualEnd < proposedStart) || (proposedEnd < actualEnd && proposedEnd < actualStart));

		});
	}
	return isValid;
}