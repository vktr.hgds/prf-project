module.exports = {

	//iterate over object
	normalizeErrors: function(errors) {

		var normalizeErrorArray = [];

		for (var element in errors) {
			if (errors.hasOwnProperty(element)){
				normalizeErrorArray.push({title: element, detail: errors[element].message});
			}
		}


		return normalizeErrorArray;
	}
}