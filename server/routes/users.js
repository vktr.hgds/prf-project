const express = require('express');
const User = require('../controllers/user')
const router = express.Router();

router.post('/auth', User.auth);



//register
router.post('/register', User.register);

module.exports = router;


//separte routes from route handlers -> controller folder with user.js file