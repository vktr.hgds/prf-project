const express = require('express');
const router = express.Router();
const Rental = require('../models/rental');
const User = require('../models/user');

const UserController = require('../controllers/user');

//just for checking jwt (token)
router.get('/secret', UserController.authMiddleware, function(req,res) {
  return res.json({"secret": true});
});


router.post('', UserController.authMiddleware, function(req, res){
  const {title, city, street, category, brand, image, description, dailyRate } = req.body;
  const user = res.locals.user;
  const rental = new Rental({title, city, street, category, brand, image, description, dailyrate});
  rental.user = user;

  Rental.create(rental, function(err, newRental) {
    if (err) {
      return res.status(422).send({errors: [{title: 'Rental Error!', detail: 'Could not create rental'}]});
    }

    User.update({_id: user.id}, {$push: {rentals: newRental}}, function(){});
    return res.json(newRental);

  })
});



router.get('', function(req, res) {

  //dont need booking when get the rentals (for example: index page)
  Rental.find({})
        .select('-bookings')
        .exec(function(err, foundRentals) {
        
        res.json(foundRentals);
  });
});



//get by id
router.get('/:id', function(req, res) {
  const rentalId = req.params.id;

  Rental.findById(rentalId)
        //dont send id
        .populate('user', 'username -_id')
        .populate('bookings', 'startAt endAt -_id')
        .exec(function(err, foundRental){
         if (err) {
            return res.status(422).send({errors: [{title: 'Rental Error!', detail: 'Could not find Rental!'}]});
         }
         res.json(foundRental);
  });
});

module.exports = router;


